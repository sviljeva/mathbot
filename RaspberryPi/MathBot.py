import ContinuousServoMotor
import time

class MathBot:
    def __init__(self, pin, step):
        self.CServoMotor = ContinuousServoMotor.ContinuousServoMotor(pin, step)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.CServoMotor.__exit__(1,2,3)

    def set_speeds(self, v):
        if v >= 0:
            self.CServoMotor.set_speed_forward(v)
        else:
            self.CServoMotor.set_speed_backward(-v)

if __name__ == '__main__':
    with MathBot(13, 50) as mathBot:
        mathBot.set_speeds(10)
        print(10)
        time.sleep(2)

        mathBot.set_speeds(70)
        print(70)
        time.sleep(2)

        mathBot.set_speeds(-10)
        print(-10)
        time.sleep(2)
        
        mathBot.set_speeds(-70)
        print(-70)
        time.sleep(2)