import time
import pigpio 
 
LED_PIN = 13
step = 50

def main():
    global pi
    #connect to pigpiod daemon
    pi = pigpio.pi()
    
    # setup pin as an output
    pi.set_mode(LED_PIN, pigpio.OUTPUT)

def move():
    for i in range(1500, 2501, step):
        pi.set_servo_pulsewidth(LED_PIN, i)
        print(i)
        time.sleep(0.5)

    for i in range(1500, 499, -step):
        pi.set_servo_pulsewidth(LED_PIN, i)
        print(i)
        time.sleep(0.5)

if __name__ == '__main__':
    main()
    move()
    #cleanup
    pi.set_mode(LED_PIN, pigpio.INPUT)
    #disconnect
    pi.stop()