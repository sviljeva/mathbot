#include "IRSensor.h"
#include "Arduino.h"

IRSensor::IRSensor(int isObstaclePin, int isObstacle) {
  this->isObstaclePin = isObstaclePin;
  this->isObstacle = isObstacle;
}

void IRSensor::setup() {
  pinMode(this->isObstaclePin, INPUT);
  Serial.begin(9600);
}

void IRSensor::loop() {
  isObstacle = digitalRead(isObstaclePin);
  
  if (isObstacle == LOW)
  {
    Serial.println("Object detected");
  }
  else
  {
    Serial.println("Object not detected");
  }
  
  delay(200);
}
