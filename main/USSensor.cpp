#include "USSensor.h"
#include "Arduino.h"
#include<math.h>

void USSensor::setup(){
	Serial.begin(9600);
}

void USSensor::loop(){
  delay(200);
  int time_taken=sensor->ping();
  
  float v=(time_taken/US_ROUNDTRIP_CM);

  Serial.println(intensity(v));
}

int USSensor::intensity(float v){
  //float a = -1.275; // na razlici 2m
  float a = -2.55; //na razlici 1m
 // float a = -0.6375; //na razlici 4m
  float b = 255;
  int f = round(a*v+b);

  if (f < 0) return 0;
  if (f > 255) return 255;
  return f;
}

USSensor::USSensor(int pinEcho,int pinTrig, int maxDist){
	this->pinEcho=pinEcho;
	this->pinTrig=pinTrig;
  this->maxDist=maxDist;
	sensor = new NewPing(pinTrig, pinEcho, maxDist);
}
