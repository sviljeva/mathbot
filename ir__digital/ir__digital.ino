int isObstaclePin = 7;  // input pin
int isObstacle = HIGH;  // HIGH nema objekta u blizini

void setup() {
  pinMode(3,OUTPUT);
  pinMode(isObstaclePin, INPUT);
  Serial.begin(9600);
  
}

void loop() {
  isObstacle = digitalRead(isObstaclePin);
  if (isObstacle == LOW)
  {
    Serial.println("Object detected");
    digitalWrite(7,HIGH);
  }
  else
  {
    Serial.println("Object not detected");
    digitalWrite(7,LOW);
  }
  delay(200);
}
