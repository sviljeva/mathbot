#include <NewPing.h>

class USSensor{
  public:
    int pinEcho;
    int pinTrig;
    int maxDist;
    NewPing* sensor;

    USSensor(int pinEcho,int pinTrig, int maxDist);

    void setup();
    void loop();
    int intensity(float v);
};
