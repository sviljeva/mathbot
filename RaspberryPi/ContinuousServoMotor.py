import time
import pigpio 

class ContinuousServoMotor:
    def __init__(self, PIN, step):
        self.PIN = PIN
        self.step = step
        self.pi = pigpio.pi()

        self.pi.set_mode(self.PIN, pigpio.OUTPUT)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.pi.set_mode(self.PIN, pigpio.INPUT)
        self.pi.stop()

    def set_speed_forward(self, v):
        if v < 0 or v > 100:
            speed = 0
        else:
            speed = -10*v+1500

        self.pi.set_servo_pulsewidth(self.PIN, speed)

    def set_speed_backward(self, v):
        if v < 0 or v > 100:
            speed = 0
        else: 
            speed = 10*v+1500

        self.pi.set_servo_pulsewidth(self.PIN, speed)

if __name__ == '__main__':
    with ContinuousServoMotor(13, 50) as CServoMotor:
        for i in range(0, 101, 10):
            CServoMotor.set_speed_forward(i)
            time.sleep(0.5)

        for i in range(0, 101, 10):
            CServoMotor.set_speed_backward(i)
            time.sleep(0.5)