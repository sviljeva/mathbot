class IRSensor {
  private:
    int isObstaclePin;
    int isObstacle;
    
   public:
    IRSensor(int isObstaclePin, int isObstacle);
    void setup();
    void loop();
};
