#include "Robot.h"

Robot* MathBot = new Robot;

void setup() {
  // put your setup code here, to run once:
  MathBot->irsensor->setup();
  MathBot->ussensor->setup();
}

void loop() {
  // put your main code here, to run repeatedly:
  MathBot->irsensor->loop();
  MathBot->ussensor->loop();
}
