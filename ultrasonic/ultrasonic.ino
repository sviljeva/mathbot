#include <NewPing.h>
#include<math.h>
#define PIN_ECHO 4
#define PIN_TRIG 5
#define MAX_DIST 400
int pin=3;


NewPing sensor(PIN_TRIG, PIN_ECHO, MAX_DIST);

int intensity(float v){
  //float a = -1.275; // na razlici 2m
  float a = -2.55; //na razlici 1m
 // float a = -0.6375; //na razlici 4m
  float b = 255;
  int f = round(a*v+b);

  if (f < 0) return 0;
  if (f > 255) return 255;
  return f;
}

void setup() 
{
  Serial.begin(9600);
}

void loop()
{
  delay(200);
  int time_taken=sensor.ping();
  
  float v=(time_taken/US_ROUNDTRIP_CM);

  Serial.println(intensity(v));
  analogWrite(pin,intensity(v));
}
